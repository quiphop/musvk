chrome.tabs.onUpdated.addListener(function(){
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {url: tabs[0].url}, function(response) {
    });
  });
});

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    chrome.downloads.download({
      url: request.url,
      filename: request.name 
    });
});
