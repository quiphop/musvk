console.log("MUSVK STARTED");
var counter = 0;
function updateView(){
    var imgUrl = chrome.extension.getURL("download.gif");
    $('.actions').each(function() {
        if(!$(this).hasClass('extended')){
            $(this).addClass('extended');
            $(this).prepend( "<img src='"+imgUrl+"' width='16' height='16' class='download_button'> </img>" );
            $(this).click(function(event){
                event.preventDefault();
                var artist = $(this).closest(".info").find(".title_wrap").find('b').find('a').text(),
                songTitle = $(this).closest(".info").find(".title_wrap").find(".title").text(),
                fullName = artist + " - " + songTitle+".mp3",
                link = $(this).parent().parent().parent().find(".play_btn").find('input').val();
                $(this).attr("href", link).attr("download", fullName);
                chrome.runtime.sendMessage({url: link, name: fullName}, function(response) {
                });

                return false;
            });
        } 
        $(".download_button" ).css("margin-right","8px");
        $(".download_button" ).css("margin-top","9px");
    });

};
$('#initial_list').bind("DOMSubtreeModified",function(){
    counter++;
    if(counter % 50 == 0){
        updateView();
    }
});
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if(request.url.includes("vk.com/audios")) {
        updateView();
    }
});

